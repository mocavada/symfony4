<?php
/**
 * Created by PhpStorm.
 * User: mocavada
 * Date: 2018-09-26
 * Time: 2:12 PM
 */

namespace App\Helper;

use Psr\Log\LoggerInterface;


trait LoggerTrait
{

    /**
     * @var LoggerInterface|null
     */
    private $logger;



    /**
     * @required
     */

    public function setLogger(LoggerInterface $logger)
    {

        $this->logger = $logger;

    }

    public function logInfo(string $message, array $context = [])
    {

        if ($this->logger)
        {

            $this->logger->info($message, $context);

        }

    }

}